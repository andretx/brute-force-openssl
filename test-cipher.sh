#!/bin/bash
# andretx
echo '
╔╦╗┌─┐┌─┐┌┬┐  ╔═╗┬┌─┐┬ ┬┌─┐┬─┐  ╔═╗┌─┐┌─┐┌┐┌╔═╗╔═╗╦  
 ║ ├┤ └─┐ │   ║  │├─┘├─┤├┤ ├┬┘  ║ ║├─┘├┤ │││╚═╗╚═╗║  
 ╩ └─┘└─┘ ┴   ╚═╝┴┴  ┴ ┴└─┘┴└─  ╚═╝┴  └─┘┘└┘╚═╝╚═╝╩═╝
		[*] Author: andretx
		[*] https://bitbucket.org/andretx/
'

start_message () {
    echo ""
    echo "[TEST] $1"
}

check_exit_status () {
    status=$1
    if [ $status -ne 0 ] ; then
        echo "ERROR: status = [ $status ]"
    else
        echo "SUCCESS."
    fi
}

test_dir=./testes
mkdir -p $test_dir

pass="test-pass-#andretx"
text="#andretx Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor  #andretx"
encfile=$test_dir/encfile.txt
echo $text > $encfile

ciphers=$(openssl enc -ciphers)

echo ""
echo "##### base64 and binary ###################################"

for cifra in $ciphers ; do
    start_message "$cifra"
    openssl enc $cifra -e -a -pbkdf2 -iter 1000 -pass pass:$pass -in $encfile -out $encfile-$cifra.enc
    check_exit_status $?
    openssl enc $cifra -d -a -pbkdf2 -iter 1000 -pass pass:$pass -in $encfile-$cifra.enc -out $encfile-$cifra.dec
    check_exit_status $?
    diff $encfile $encfile-$cifra.dec
    check_exit_status $?
done

echo ""
echo "##### binary ###################################"

for cifra in $ciphers ; do
    start_message "$cifra"
    openssl enc $cifra -e -pbkdf2 -iter 1000 -pass pass:$pass -in $encfile -out $encfile-$cifra.enc
    check_exit_status $?
    openssl enc $cifra -d -pbkdf2 -iter 1000 -pass pass:$pass -in $encfile-$cifra.enc -out $encfile-$cifra.dec
    check_exit_status $?
    diff $encfile $encfile-$cifra.dec
    check_exit_status $?
done

echo ""
echo "##### base64 ###################################"

for cifra in $ciphers ; do
    start_message "$cifra"
    openssl enc $cifra -e -base64 -pbkdf2 -iter 1000 -pass pass:$pass -in $encfile -out $encfile-$cifra-b64.enc
    check_exit_status $?
    openssl enc $cifra -d -base64 -pbkdf2 -iter 1000 -pass pass:$pass -in $encfile-$cifra-b64.enc -out $encfile-$cifra-b64.dec
    check_exit_status $?
    diff $encfile $encfile-$cifra-b64.dec
    check_exit_status $?
done
# 
