#!/bin/bash
# andretx
echo '          
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::                                                                                                           
	╔═╗┌─┐┬─┐┌─┐┌─┐  ╔═╗┌─┐┌─┐┌┐┌╔═╗╔═╗╦  
	╠╣ │ │├┬┘│  ├┤   ║ ║├─┘├┤ │││╚═╗╚═╗║  
	╚  └─┘┴└─└─┘└─┘  ╚═╝┴  └─┘┘└┘╚═╝╚═╝╩═╝
	
		[*] Author: andretx
		[*] https://bitbucket.org/andretx/
	
	Test multiple passwords by combining ciphers,
	encryption options, and text search within 10 lines
	of the response to validate whether decryption was successful.
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
'

if [[ $# -le 4 ]]; then
        echo -e "USE: $0 file.enc wordlist.txt keyword options filter"
        echo -e "parameters:\n *encrypted file \n *wordlist=list of possible passwords \n *keyword=search keyword in answer \n options=additional options \n filter=specific cipher"
        echo -e "EXAMPLE: $0 file.enc rockyou.txt CHAVE cammelia\n"
else
	keyword=$3
	options='-a -salt' # default options
	if [[ -n $4 ]]; then
		options=$4
	fi

	filter='-' # default options
	if [[ -n $5 ]]; then
		filter=$5
	fi
	ciphers=$(openssl enc -ciphers | grep -i $filter)
	for cipher in ${ciphers[@]}
	do
		echo -e "TEST - $cipher ... \n\n"
		sleep 1
		echo -e "....\n"
		while read pass; do
			RET=$(openssl enc -d $cipher $options -in $1 -pass pass:$pass 2>&1 | grep -C 10 "$keyword")
			if [[ -n $RET ]]; then
				echo
				echo "SUCESS - CIPHER: $cipher - PASS: $pass"
				echo
				exit 1
			else
				echo "FAILED - $pass"
			fi
		done < "$2"
	done
fi
