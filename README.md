# BRUTE FORCE OPENSSL - Symmetric Encryption
Languages bash script:
* **GNU bash** version, version 5.0.3(1)-release-(x86_64-pc-linux-gnu)

### Encryption Test - OPENSSL
Brute force test on encrypted files using symmetric encryption and weak password using OpenSSL
- - -
* **openssl-dec.sh**
```.sh
./openssl-dec.sh file.enc wordlist.txt keyword filter
```
### Test Ciphers - OPENSSL
check ciphers available in OpenSSL
- - -
**test-cipher.sh**
```.sh
./test-cipher.sh
```

**#andretx**
